import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CallList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new ListView.builder
        (
          itemCount: 8,
          itemBuilder: (BuildContext ctxt, int index) {
            return new ListTile(
              leading: _buildAvatar(),
              title: new Text('Jenifer Lopez'),
              subtitle: new Row(children: <Widget>[
                new Text('Incoming',style: TextStyle(color: Colors.deepOrange),),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: new Text('5 minute ago'),
                ),
              ],),
              trailing: new Icon(Icons.call),

            );
          }
      ),
      floatingActionButton: _floatingActionButton() ,

    );
  }

  Widget _buildAvatar() {
    return Container(
      width: 50.0,
      height: 50.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white),
      ),
      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        child: Image.network('https://avatars1.githubusercontent.com/u/20367660?s=460&v=4'),
      ),
    );
  }


  Widget _floatingActionButton() {
    return FlatButton(
        onPressed: null,
        child: new Container(
          decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30.0)),
            boxShadow: <BoxShadow>[

              BoxShadow(
                color: const Color(0x809968f8),
                offset: Offset(1.0, 6.0),
                blurRadius: 3.0,
              ),
            ],
            gradient: new LinearGradient(
                colors: [
                  const Color(0xFF898dfb),
                  const Color(0xFF9968f8),
                ],
                begin: const FractionalOffset(0.2, 0.2),
                end: const FractionalOffset(1.0, 1.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: new Icon(
              Icons.call,
              color: Colors.white,
            ),
          ),
        ));
  }


}
