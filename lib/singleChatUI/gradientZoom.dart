import 'package:flutter/material.dart';

class GradientZoom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new Center(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Container(
              decoration: new BoxDecoration(
                //borderRadius: BorderRadius.all(Radius.circular(30.0)),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(1.0),
                    topRight: Radius.circular(20.0),
                    bottomLeft: Radius.circular(25.0),
                    bottomRight: Radius.circular(20.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: const Color(0x609968f8),
                    offset: Offset(1.0, 6.0),
                    blurRadius: 5.0,
                  ),
                ],
                gradient: new LinearGradient(
                    colors: [
                      const Color(0xFF898dfb),
                      const Color(0x809968f8),
                    ],
                    begin: const FractionalOffset(0.2, 0.2),
                    end: const FractionalOffset(1.0, 1.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: MaterialButton(
                  highlightColor: Colors.transparent,
                  splashColor: const Color(0xFF9968f8),
                  //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  child: Padding(
                    padding:
                    const EdgeInsets.symmetric(
                        vertical: 18.0, horizontal: 60.0),
                    child: Text(
                      "I'm Fine.How are you all?",
                      style: TextStyle(
                          color: Colors.white,
                          //fontSize: 18.0,
                          fontFamily: "WorkSansNormal"),
                    ),
                  ),
                  onPressed: () => print("Login button pressed")),
            ),
          ),
        ));
  }}
