import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class SingleChatUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: _buildGradientAppBar(),
        body: new ListView.builder(
            itemCount: 8,
            itemBuilder: (BuildContext ctxt, int index) {
              return new Column(
                children: <Widget>[
                  _chatBoxLeft(),
                  _buildChatBoxRight(),
                ],
              );
            }),
        bottomSheet: buildInput(),
      ),
    );
  }

  Widget _buildGradientAppBar() {
    return new GradientAppBar(
      backgroundColorStart: Color(0xFF9968f8),
      backgroundColorEnd: Color(0xFF898dfb),
      leading: new Icon(
        Icons.arrow_back,
      ),
      title: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text(
            'Nazirul Hoque',
            style: TextStyle(fontSize: 18.0),
          ),
          new Text(
            'Active Now',
            style: TextStyle(fontSize: 12.0),
          ),
        ],
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: new Icon(Icons.call),
        ),
        Padding(
          padding: const EdgeInsets.only(
            left: 10.0,
            right: 8.0,
          ),
          child: new Icon(Icons.videocam),
        ),
      ],
//      bottom: ,
    );
  }

  Widget _chatBoxLeft() {
    return new ListTile(
        leading: _buildAvatar(),
        title: new Padding(
          padding: EdgeInsets.only(top: 5.0, right: 80.0),
          child: _buildChatBoxLeft(),
        ),
        subtitle: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: new Text('3:04 AM'),
            ),
          ],
        ));
  }

  Widget _buildAvatar() {
    return Container(
      width: 50.0,
      height: 50.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white),
      ),
//      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        child: Image.asset('assets/images/avatar.png'),
      ),
    );
  }

  Widget _buildChatBoxLeft() {
    return Container(
      decoration: new BoxDecoration(
        //borderRadius: BorderRadius.all(Radius.circular(30.0)),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(1.0),
            topRight: Radius.circular(20.0),
            bottomLeft: Radius.circular(25.0),
            bottomRight: Radius.circular(20.0)),

        gradient: new LinearGradient(
            colors: [
              const Color(0xFF898dfb),
              const Color(0x809968f8),
            ],
            begin: const FractionalOffset(0.2, 0.2),
            end: const FractionalOffset(1.0, 1.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
      ),
      child: MaterialButton(
        highlightColor: Colors.transparent,
        splashColor: const Color(0xFF9968f8),
        //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                "I'm Fine.How are you all?,Hey What's up ? We will talk tomorrow ",
                style: TextStyle(
                    color: Colors.white, fontFamily: "WorkSansNormal"),
              ),
            ),
            new Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Icon(
                    Icons.done_all,
                    size: 12.0,
                    color: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    'Seen 5.01 PM',
                    style: TextStyle(fontSize: 12.0, color: Colors.white),
                  ),
                )
              ],
            ),
          ],
        ),
        onPressed: () {},
      ),
    );
  }

  Widget _buildChatBoxRight() {
    return Padding(
      padding: const EdgeInsets.only(
          left: 100.0, right: 15.0, top: 5.0, bottom: 5.0),
      child: Container(
        decoration: new BoxDecoration(
          //borderRadius: BorderRadius.all(Radius.circular(30.0)),
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              topRight: Radius.circular(1.0),
              bottomLeft: Radius.circular(25.0),
              bottomRight: Radius.circular(20.0)),
          color: new Color(0X80989898),
        ),
        child: MaterialButton(
          highlightColor: Colors.transparent,
          splashColor: const Color(0x30989898),
          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  "I'm Fine.How are you all?,Hey What's up ? We will talk tomorrow ",
                  style: TextStyle(
                      color: Colors.black87, fontFamily: "WorkSansNormal"),
                ),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Icon(
                      Icons.done_all,
                      size: 12.0,
                      color: Colors.black45,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: Text(
                      'Seen 5.01 PM',
                      style: TextStyle(fontSize: 12.0, color: Colors.black45),
                    ),
                  )
                ],
              ),
            ],
          ),
          onPressed: () {},
        ),
      ),
    );
  }

  Widget buildInput() {
    return Container(
      child: Row(
        children: <Widget>[
          // Button send image
          Material(
            child: new Container(
              margin: new EdgeInsets.symmetric(horizontal: 1.0),
              child: new IconButton(
                icon: new Icon(Icons.attach_file),
                onPressed: () {},
                color: Colors.grey,
              ),
            ),
            color: Colors.white,
          ),

          // Edit text
          Flexible(
            child: Container(
              child: TextField(
                style: TextStyle(color: Colors.green, fontSize: 15.0),
                decoration: InputDecoration.collapsed(
                  hintText: 'Type a message...',
                  hintStyle: TextStyle(color: Colors.grey),
                ),
              ),
            ),
          ),
          // Button send message
          Material(
            child: new Container(
              child: new IconButton(
                icon: new Icon(Icons.face),
                onPressed: null,
                color: Colors.green,
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: new Container(
              child: new IconButton(
                icon: new Icon(Icons.camera_alt),
                onPressed: () {},
                color: Colors.black45,
              ),
            ),
            color: Colors.white,
          ),
          Material(
            child: new Container(
              child: new IconButton(
                icon: new Icon(Icons.mic),
                onPressed: () {},
                color: Colors.black45,
              ),
            ),
            color: Colors.white,
          ),


          // Button send message
        ],
      ),
      width: double.infinity,
      height: 50.0,
      decoration: new BoxDecoration(
          border:
              new Border(top: new BorderSide(color: Colors.grey, width: 0.5)),
          color: Colors.white),
    );
  }
}
