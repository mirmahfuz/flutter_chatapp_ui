import 'package:flutter/material.dart';

class TabChoice {
  //Tab Element
  const TabChoice({this.title, this.icon});
  final String title;
  final IconData icon;
}

