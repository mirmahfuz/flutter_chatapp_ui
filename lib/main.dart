import 'package:flutter/material.dart';
import 'package:flutter_chatapp_ui/callList.dart';
import 'package:flutter_chatapp_ui/chatList.dart';
import 'package:flutter_chatapp_ui/contactList.dart';
import 'package:flutter_chatapp_ui/singleChatUI/singleChatUI.dart';
import 'package:flutter_chatapp_ui/tabChoice.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

void main() => runApp(
    //SingleChat UI
    SingleChatUI());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: choices.length,
        child: Scaffold(
          appBar: _buildGradientAppBar(),
          body: _buildTabBarView(),
        ),
      ),
    );
  }

  Widget _buildGradientAppBar() {
    return new GradientAppBar(
      backgroundColorStart: Color(0xFF9968f8),
      backgroundColorEnd: Color(0xFF898dfb),
      title: Padding(
        padding: const EdgeInsets.only(left: 15.0,top: 5.0),
        child: new Image.asset(
          'assets/images/logo.png',
          width: 150.0,
        ),
      ),
      leading: new Icon(
        Icons.more_vert,
      ),
      actions: <Widget>[new Icon(Icons.search)],
      bottom: TabBar(
        indicatorColor: Colors.white,
        indicatorSize: TabBarIndicatorSize.tab,

        //isScrollable: true,
        tabs: choices.map((TabChoice choice) {
          return Tab(
            text: choice.title,
            //icon: Icon(choice.icon),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildTabBarView() {
    return TabBarView(
      children: <Widget>[
        new ChatList(),
        new CallList(),
        new ContactList(),
      ],
    );
  }

}

const List<TabChoice> choices = const <TabChoice>[
  //Details element of Tab Bar
  const TabChoice(title: 'Chat', icon: Icons.done),
  const TabChoice(title: 'Call', icon: Icons.done),
  const TabChoice(title: 'Contacts', icon: Icons.done),
];
